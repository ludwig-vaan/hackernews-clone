# Project Name :
TO READ :
https://blog.apollographql.com/introducing-react-apollo-2-1-c837cc23d926

TUTORIAL : 
https://www.howtographql.com/react-apollo

## Front
```sh
yarn start
```
Runs the app in the development mode.<br>
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

## Server :
Your Prisma endpoint is live:

  HTTP:  https://us1.prisma.sh/vantoursludwig/ludwig/graphy
  WS:    wss://us1.prisma.sh/vantoursludwig/ludwig/graphy

You can view & edit your data here:

  Prisma Admin: https://us1.prisma.sh/vantoursludwig/ludwig/graphy/_admin

### Exploring the server
```sh
cd server
yarn start
```

Open [http://localhost:4000](http://localhost:4000) to view GraphQL Playground for you to explore and work with the API.

>A Playground is a “GraphQL IDE”, providing an interactive environment that allows to send queries, mutations and subscriptions to your GraphQL API. It is similar to a tool like Postman which you might know from working with REST APIs, but comes with a lot of additional benefits.

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.<br>
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br>
You will also see any lint errors in the console.

### `npm test`

Launches the test runner in the interactive watch mode.<br>
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `npm run build`

Builds the app for production to the `build` folder.<br>
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.<br>
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.