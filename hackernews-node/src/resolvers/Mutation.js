const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const { APP_SECRET, getUserId } = require('../utils');

const post = (root, args, context) => {
  const userId = getUserId(context);

  if (!userId) throw new Error('Not authorize');

  return context.prisma.createLink({
    ...args,
    postedBy: { connect: { id: userId } },
  });
};

const updateLink = (root, args, context) => {
  const { id } = args;
  delete args.id;
  return context.prisma.updateLink({
    data: { ...args },
    where: { id },
  });
};

const deleteLink = (parent, { id }, context) => context.prisma.deleteLink({
  id,
});

const signup = async (parent, { email, password, name }, context) => {
  const bcryptPassword = await bcrypt.hash(password, 10);
  const user = await context.prisma.createUser({
    email,
    name,
    password: bcryptPassword,
  });
  const token = jwt.sign({ userId: user.id }, APP_SECRET);

  return { token, user };
};

const login = async (root, { email, password }, context) => {
  const user = await context.prisma.user({ email });
  if (!user) throw new Error('no such user found');
  const correctPassword = await bcrypt.compare(password, user.password);
  if (!correctPassword) throw new Error('Invalid password');

  const token = jwt.sign({ userId: user.id }, APP_SECRET);

  return { token, user };
};

const vote = async (root, args, context, info) => {
  const userId = getUserId(context);

  const linkExists = await context.prisma.$exists.vote({
    user: { id: userId },
    link: { id: args.linkId },
  });

  if (linkExists) {
    throw new Error(`Already voted for link: ${args.linkId}`);
  }
  return context.prisma.createVote({
    user: { connect: { id: userId } },
    link: { connect: { id: args.linkId } },
  });
};

module.exports = {
  post,
  updateLink,
  deleteLink,
  login,
  signup,
  vote,
};
