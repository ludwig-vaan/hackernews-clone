async function feed(parent, args, context) {
  const where = args.filter
    ? {
      OR: [{ description_contains: args.filter }, { url_contains: args.filter }],
    }
    : {};

  const links = await context.prisma.links({
    where,
    skip: args.skip,
    first: args.first,
    orderBy: args.orderBy,
  });
  const count = await context.prisma
    .linksConnection({
      where,
    })
    .aggregate()
    .count();
    console.log('====================================');
    console.log(links);
    console.log('====================================');
  return {
    links,
    count,
  };
}

const info = () => 'Hello graphQL world !';

const link = (root, args, context) => context.prisma.links().find(currentLink => currentLink.id === args.id);

module.exports = {
  feed,
  info,
  link,
};
