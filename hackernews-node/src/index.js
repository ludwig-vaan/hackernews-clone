const { GraphQLServer } = require('graphql-yoga');
const { prisma } = require('./generated/prisma-client');
const Query = require('./resolvers/Query');
const Mutation = require('./resolvers/Mutation');
const User = require('./resolvers/User');
const Link = require('./resolvers/Link');
const Vote = require('./resolvers/Vote');
const Subscription = require('./resolvers/Subscription');

/**
 * TODO :
 * https://www.howtographql.com/graphql-js/6-authentication/
 * https://www.prisma.io/docs/1.33/prisma-client/basic-data-access/writing-data-JAVASCRIPT-rsc6/#nested-object-writes
 *
 * In general, when adding a new feature to the API, the process will look pretty similar
 * every time:
 *
 * Extend the GraphQL schema definition with a new root field (and new data types, if needed)
 * Implement corresponding resolver functions for the added fields
 *
 * This process is also referred to as schema-driven or schema-first development.
 */

const resolvers = {
  Query,
  Mutation,
  User,
  Link,
  Vote,
  Subscription,
};

const server = new GraphQLServer({
  typeDefs: './src/schema.graphql',
  resolvers,
  context: request => ({
    ...request,
    prisma,
  }),
});

server.start(() => console.log('Server is running on http://localhost:4000 ✨'));
