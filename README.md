# GRAPHQL PROJECT

## Getting Started

1. Clone repository

```
git clone
```

2. Install dependencies & Deploy the Prisma database API

```sh
// BACK
cd ../hackernews-node
npm install
npm prisma deploy
```

Then, follow these steps in the interactive CLI wizard: 1. Select Demo server 2. Authenticate with Prisma Cloud in your browser (if necessary) 2. Back in your terminal, confirm all suggested values

```sh
npm start
```

> If you want to interact with the GraphQL API of the server inside a
> GraphQL Playground, you can navigate to http://localhost:4000.

```sh
// FRONT
cd hackernews-react-apollo
yarn install
yarn start
```

You can now open your browser and use the app on http://localhost:3000.

## FRONT : hackernew-react-apollo

A simple clone of Hackernews with React & Apollo Client

Features :

- Display a list of links
- Search the list of links
- Users can authenticate
- Authenticated users can create new links
- Authenticated users can upvote links (one vote per link and user)
- Realtime updates when other users upvote a link or create a new one

## BACK : hackernews-node

GraphQL server with Node.js, graphql-yoga and Prisma

- **graphql-yoga** is a fast and simple GraphQL server library built on top of Express.js. It comes with several features, such as out-of-the-box support for GraphQL Playgrounds and realtime GraphQL subscriptions.

- **Prisma** The resolvers of your GraphQL server are implemented using the Prisma client that’s responsible for database access.
